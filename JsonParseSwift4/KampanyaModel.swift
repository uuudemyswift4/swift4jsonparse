//
//  KampanyaModel.swift
//  JsonParseSwift4
//
//  Created by Kerim Çağlar on 07/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import Foundation

//Swift 4 ile Codable sayesinde json decode/encode kullanarak işlemlerimiz kolaylaştı
struct Kampanya:Codable{
    var baslik:String
}
