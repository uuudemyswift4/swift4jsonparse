//
//  JsonModel.swift
//  JsonParseSwift4
//
//  Created by Kerim Çağlar on 07/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import Foundation

//Swift 4 ile Json işlemleri çooooook kolayalaştı
struct Kampanya:Codable{
    var baslik:String
    var icerik:String
}
