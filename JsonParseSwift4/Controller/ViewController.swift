//
//  ViewController.swift
//  JsonParseSwift4
//
//  Created by Kerim Çağlar on 07/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

let myUrl = "http://www.bucayapimarket.com/json.php"
let url = URL(string:myUrl)! // myUrl stringini url e dönüştürdü
let myData = try! Data(contentsOf:url)
var jsonDecoder = JSONDecoder()

var basliklar = [String]() //Elemanları String türünde olacak boş bir array tanımladık

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let kampanyalar = try? jsonDecoder.decode([Kampanya].self, from: myData)
        
        //dump(kampayalar?.first)
        
        if let kampanya = kampanyalar {
            //print(kampanya)
            for k in kampanya{
                basliklar.append(k.baslik)
            }
            
        }
    }
    
    //TableView Methodları
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return basliklar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        
        cell.textLabel?.text = basliklar[indexPath.row]
        
        return cell
    }
    
    

}

